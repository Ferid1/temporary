<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tbl_rules;
use App\Tbl_domains;

class ApiController extends Controller
{
    public function allDomains(Request $request)
    {
        $domain = Tbl_domains::all();
        return response()->json(['domain' => $domain]);
    }

    public function singleDomain(Request $request)
    {
        $domain = Tbl_domains::select("*")
                           ->where('id', '=', $request->id)
                           ->first();
        return response()->json(['domain' => $domain]);
    }


    public function allRules(Request $request)
    {
        $rule = Tbl_rules::all();
        return response()->json(['domain' => $rule]);
    }

    public function singleRule(Request $request)
    {
        $rule = Tbl_rules::select("*")
                           ->where('id', '=', $request->id)
                           ->first();
        return response()->json(['domain' => $rule]);
    }
}
