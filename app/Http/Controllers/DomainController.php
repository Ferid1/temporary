<?php

namespace App\Http\Controllers;

use App\Tbl_domains;
use Illuminate\Http\Request;

class DomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // die('aa');
        // $background = Backgrounds::all();
        return view( 'index');
    }

    public function domain()
    {
        $domains = Tbl_domains::all();
        // echo $domains;
        // die();
        return view( 'domains/domains',['domains' => $domains]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo $request->domain;
        // echo "0";
        // die();
        Tbl_domains::insert(
            ['name' => $request->domain]
          );
        return redirect( 'domains/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function show(Domain $domain)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function edit(Domain $domain)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Tbl_domains::where('id', $request->id)->update(['name' => $request->domain]);
        return redirect( 'domains/');

    }

    public function showEdit(Request $request)
    {
        $id = $request->id;
        $domain = Tbl_domains::select("*")
                           ->where('id', '=', $id)
                           ->first();

        return view( 'domains/edit-domains',['domain' => $domain]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Tbl_domains::where('id',$id)->delete();
        return redirect( 'domains/');
    }
}
