<?php

namespace App\Http\Controllers;

use App\Tbl_rules;
use App\Tbl_domains;
use Illuminate\Http\Request;
use File;

class TblRulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rules = Tbl_rules::all();
        $domains = Tbl_domains::all();
       
        return view( 'rules/rules',['rules' => $rules, 'domains' => $domains]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Tbl_rules::insert([
            'domain_id' => $request->domain_id,
            'rule_title' => $request->title,
            'rule_short_content' => $request->content,
            'rule_images' => $request->styled_file,
            'rule_author' => $request->author,
            'rule_date_full' => $request->date
        ]);

    
        return redirect( 'rules/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tbl_rules  $tbl_rules
     * @return \Illuminate\Http\Response
     */
    public function show(Tbl_rules $tbl_rules)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tbl_rules  $tbl_rules
     * @return \Illuminate\Http\Response
     */
    public function edit(Tbl_rules $tbl_rules,$id)
    {
        // echo $id;
        // $id = $tbl_rules->id;
        // echo $id;
        $rules = Tbl_rules::select("*")
                           ->where('id', '=', $id)
                           ->first();

        $domains = Tbl_domains::all();     
        // echo $rules->id;
        // echo $domains;
        // die();
        return view( 'rules/edit-rules',['rules' => $rules, 'domains' => $domains]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tbl_rules  $tbl_rules
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tbl_rules $tbl_rules)
    {

        // echo $request->id."<br>";
        // echo $request->short_content."<br>";
        // echo $request->styled_file."<br>";
        // echo $request->author."<br>";
        // echo $request->full_date."<br>";
        // echo $request->id."<br>";
        // die('-');



        Tbl_rules::where('id', $request->id)->update([
            'rule_title' => $request->title,
            'rule_short_content' => $request->short_content,
            // 'rule_images' => $request->styled_file,
            'rule_author' => $request->author,
            'rule_date_full' => $request->full_date
            ]);
        return redirect( 'rules/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tbl_rules  $tbl_rules
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Tbl_rules::where('id',$id)->delete();
        return redirect( 'rules/');
    }
}
