<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('domain_id')->nullable();
            $table->text('rule_title');
            $table->text('rule_short_content');
            $table->text('rule_images')->nullable();
            $table->text('rule_author')->nullable();
            $table->string('rule_date_year', 200)->nullable();
            $table->string('rule_date_month', 200)->nullable();
            $table->string('rule_date_day', 200)->nullable();
            $table->string('rule_date_full', 200)->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->foreign('domain_id')->references('id')->on('tbl_domains');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_rules');
    }
}
