<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Pages</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/fonts/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{ asset('js/custom/jquery.min.js') }}"></script>
    <script src="{{ asset('js/custom/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/plugins/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{ asset('js/plugins/visualization/d3.min.js') }}"></script>
    <script src="{{ asset('js/plugins/visualization/d3_tooltip.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/switchery.min.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/bootstrap_multiselect.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/validation/validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/touchspin.min.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/select2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/switch.min.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/uniform.min.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/inputmask.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/autosize.min.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/form_validation.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/form_controls_extended.js') }}"></script>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/plugins/dashboard.js') }}"></script>
    <!-- /theme JS files -->

</head>

<body>

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">
        <a href="index.html" class="d-inline-block">
            <img src="/static/images/logo_light.png" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>
</div>
<!-- /main navbar -->


<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            MenuBar
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- User menu -->
            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media">

                        <div class="media-body">
                            <div class="media-title font-weight-semibold">Victoria Baker</div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /user menu -->


            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Main -->
                    <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Əsas menu</div> <i class="icon-menu" title="Main"></i></li>
                    <li class="nav-item">
                        <a href="/" class="nav-link {% if context.active_menu == 'index' %} active {% endif %}">
                            <i class="icon-home4"></i>
                            <span>Topics</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/domains" class="nav-link {% if context.active_menu == 'domains' %} active {% endif %}">
                            <i class="icon-stack"></i>
                            <span>Domains</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/packages" class="nav-link {% if context.active_menu == 'packages' %} active {% endif %}">
                            <i class="icon-color-sampler"></i>
                            <span>Packages</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/rules" class="nav-link {% if context.active_menu == 'rules' %} active {% endif %}">
                            <i class="icon-color-sampler"></i>
                            <span>Rules</span>
                        </a>
                    </li>
                    <!-- /main -->

                </ul>
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->


 @yield('content')

</div>
<!-- /page content -->


</body>
</html>
