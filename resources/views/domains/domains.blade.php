@extends('base')

@section('content')
    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><span class="font-weight-semibold">Topic</span> - Page</h4>
                </div>
            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Topic</a>
                        <span class="breadcrumb-item active">Page</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page header -->

        <div class="content">
            <!-- Simple lists -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Dropdown list -->
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Mövcud Level</h5>
                        </div>

                        <div class="card-body">
                            <ul class="media-list">
                            @foreach($domains as $data)
                                <li class="media justify-content-between">
                                    <div class="box-content">
                                        <span class="mr-3">
                                            <a href="#">
                                                <a href="{{ url('domains/'.$data->id)}}" class="row-level">#{{$data->id}}</a>
                                               
                                            </a>
                                        </span>

                                        <span class="media-body">
                                             <a href="{{ url('domains/'.$data->id)}}"><span class="media-title font-weight-semibold">{{$data->name}}</span></a>
                                        </span> 
                                    </div>
                                    <div class="box-buttons">
                                        <span class="mr-2">
                                            <a href="{{ url('domains/'.$data->id)}}/edit">
                                                <i class="icon-pencil7 icon-1x" data-popup="tooltip" title="" data-original-title="Redaktə et"></i>
                                            </a>
                                        </span>
                                        <span class="mr-2">
                                            <a href="{{ url('domains/remove/'.$data->id)}}">
                                                   
                                                <i class="icon-trash icon-1x text-danger"  data-popup="tooltip" title="" data-original-title="Sil"></i>
                                            </a>
                                        </span>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /dropdown list -->

                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-white header-elements-inline">
                            <h6 class="card-title">Yeni Page</h6>
                        </div>
                        <div class="card-body">
                            <form action="{{ action('DomainController@store') }}" method="post">
                            {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Domain name</label>
                                            <input type="text" class="form-control" name="domain">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn bg-teal">Yeni Page</button>
                                    </div>
                                </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /simple lists -->
        </div>

        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text text-center">&copy; 2019</span>
            </div>
        </div>
        <!-- /footer -->

    </div>
    <!-- /main content -->
    

@endsection