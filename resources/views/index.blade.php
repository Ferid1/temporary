
@extends('base')

@section('content')

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><span class="font-weight-semibold">Topic</span></h4>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="index.html" class="breadcrumb-item active"><i class="icon-home2 mr-2"></i>Topic</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <div class="content">
        <!-- Simple lists -->
        <div class="row">

            <div class="col-md-6">

                <!-- Dropdown list -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Mövcud Topiclər</h5>
                    </div>

                    <div class="card-body">
                        <ul class="media-list">

                            <li class="media justify-content-between">
                                <div class="box-content">
                                    <span class="mr-3">
                                        <a href="levels.html">
                                            <i class="icon-books text-info icon-1_5x "></i>
                                        </a>
                                    </span>

                                    <span class="media-body">
                                        <a href="levels.html"><span class="media-title font-weight-semibold">James Alexander</span></a>
                                    </span>
                                </div>
                                <div class="box-buttons">
                                    <span class="mr-2">
                                        <a href="#" data-toggle="modal" data-target="#modal_small">
                                            <i class="icon-pencil7 icon-1x" data-popup="tooltip" title="" data-original-title="Redaktə et"></i>
                                        </a>
                                    </span>
                                    <span class="mr-2">
                                        <a href="#">
                                            <i class="icon-trash icon-1x text-danger"  data-popup="tooltip" title="" data-original-title="Sil"></i>
                                        </a>
                                    </span>
                                </div>
                            </li>

                            <li class="media justify-content-between">
                                <div class="box-content">
                                    <span class="mr-3">
                                        <a href="levels.html">
                                            <i class="icon-books text-info icon-1_5x "></i>
                                        </a>
                                    </span>

                                    <span class="media-body">
                                        <a href="levels.html"><span class="media-title font-weight-semibold">James Alexander</span></a>
                                    </span>
                                </div>
                                <div class="box-buttons">
                                    <span class="mr-2">
                                        <a href="#" data-toggle="modal" data-target="#modal_small">
                                            <i class="icon-pencil7 icon-1x" data-popup="tooltip" title="" data-original-title="Redaktə et"></i>
                                        </a>
                                    </span>
                                    <span class="mr-2">
                                        <a href="#">
                                            <i class="icon-trash icon-1x text-danger"  data-popup="tooltip" title="" data-original-title="Sil"></i>
                                        </a>
                                    </span>
                                </div>
                            </li>

                            <li class="media justify-content-between">
                                <div class="box-content">
                                    <span class="mr-3">
                                        <a href="levels.html">
                                            <i class="icon-books text-info icon-1_5x "></i>
                                        </a>
                                    </span>

                                    <span class="media-body">
                                        <a href="levels.html"><span class="media-title font-weight-semibold">James Alexander</span></a>
                                    </span>
                                </div>
                                <div class="box-buttons">
                                    <span class="mr-2">
                                        <a href="#" data-toggle="modal" data-target="#modal_small">
                                            <i class="icon-pencil7 icon-1x" data-popup="tooltip" title="" data-original-title="Redaktə et"></i>
                                        </a>
                                    </span>
                                    <span class="mr-2">
                                        <a href="#">
                                            <i class="icon-trash icon-1x text-danger"  data-popup="tooltip" title="" data-original-title="Sil"></i>
                                        </a>
                                    </span>
                                </div>
                            </li>

                            <li class="media justify-content-between">
                                <div class="box-content">
                                    <span class="mr-3">
                                        <a href="levels.html">
                                           <i class="icon-books text-info icon-1_5x "></i>
                                        </a>
                                    </span>

                                    <span class="media-body">
                                        <a href="levels.html"><span class="media-title font-weight-semibold">James Alexander</span></a>
                                    </span>
                                </div>
                                <div class="box-buttons">
                                    <span class="mr-2">
                                        <a href="#" data-toggle="modal" data-target="#modal_small">
                                            <i class="icon-pencil7 icon-1x" data-popup="tooltip" title="" data-original-title="Redaktə et"></i>
                                        </a>
                                    </span>
                                    <span class="mr-2">
                                        <a href="#">
                                            <i class="icon-trash icon-1x text-danger"  data-popup="tooltip" title="" data-original-title="Sil"></i>
                                        </a>
                                    </span>
                                </div>
                            </li>

                            <li class="media justify-content-between">
                                <div class="box-content">
                                    <span class="mr-3">
                                        <a href="levels.html">
                                           <i class="icon-books text-info icon-1_5x "></i>
                                        </a>
                                    </span>

                                    <span class="media-body">
                                        <a href="levels.html"><span class="media-title font-weight-semibold">James Alexander</span></a>
                                    </span>
                                </div>
                                <div class="box-buttons">
                                    <span class="mr-2">
                                        <a href="#" data-toggle="modal" data-target="#modal_small">
                                            <i class="icon-pencil7 icon-1x" data-popup="tooltip" title="" data-original-title="Redaktə et"></i>
                                        </a>
                                    </span>
                                    <span class="mr-2">
                                        <a href="#">
                                            <i class="icon-trash icon-1x text-danger"  data-popup="tooltip" title="" data-original-title="Sil"></i>
                                        </a>
                                    </span>
                                </div>
                            </li>

                            <li class="media justify-content-between">
                                <div class="box-content">
                                    <span class="mr-3">
                                        <a href="levels.html">
                                           <i class="icon-books text-info icon-1_5x "></i>
                                        </a>
                                    </span>

                                    <span class="media-body">
                                        <a href="levels.html"><span class="media-title font-weight-semibold">James Alexander</span></a>
                                    </span>
                                </div>
                                <div class="box-buttons">
                                    <span class="mr-2">
                                        <a href="#" data-toggle="modal" data-target="#modal_small">
                                            <i class="icon-pencil7 icon-1x" data-popup="tooltip" title="" data-original-title="Redaktə et"></i>
                                        </a>
                                    </span>
                                    <span class="mr-2">
                                        <a href="#">
                                            <i class="icon-trash icon-1x text-danger"  data-popup="tooltip" title="" data-original-title="Sil"></i>
                                        </a>
                                    </span>
                                </div>
                            </li>

                            <li class="media justify-content-between">
                                <div class="box-content">
                                    <span class="mr-3">
                                        <a href="levels.html">
                                            <i class="icon-books text-info icon-1_5x "></i>
                                        </a>
                                    </span>

                                    <span class="media-body">
                                        <a href="levels.html"><span class="media-title font-weight-semibold">James Alexander</span></a>
                                    </span>
                                </div>
                                <div class="box-buttons">
                                    <span class="mr-2">
                                        <a href="#" data-toggle="modal" data-target="#modal_small">
                                            <i class="icon-pencil7 icon-1x" data-popup="tooltip" title="" data-original-title="Redaktə et"></i>
                                        </a>
                                    </span>
                                    <span class="mr-2">
                                        <a href="#">
                                            <i class="icon-trash icon-1x text-danger"  data-popup="tooltip" title="" data-original-title="Sil"></i>
                                        </a>
                                    </span>
                                </div>
                            </li>

                            <li class="media justify-content-between">
                                <div class="box-content">
                                    <span class="mr-3">
                                        <a href="levels.html">
                                            <i class="icon-books text-info icon-1_5x "></i>
                                        </a>
                                    </span>

                                    <span class="media-body">
                                        <a href="levels.html"><span class="media-title font-weight-semibold">James Alexander</span></a>
                                    </span>
                                </div>
                                <div class="box-buttons">
                                    <span class="mr-2">
                                        <a href="#" data-toggle="modal" data-target="#modal_small">
                                            <i class="icon-pencil7 icon-1x" data-popup="tooltip" title="" data-original-title="Redaktə et"></i>
                                        </a>
                                    </span>
                                    <span class="mr-2">
                                        <a href="#">
                                            <i class="icon-trash icon-1x text-danger"  data-popup="tooltip" title="" data-original-title="Sil"></i>
                                        </a>
                                    </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /dropdown list -->

            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">Yeni Topic</h6>
                    </div>

                    <div class="card-body">
                        <form action="#">
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Topic adı:</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control border-slate border-1" placeholder="Futbol">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Aforizm:</label>
                                <div class="col-lg-10">
                                    <input type="file" name="styled_file" class="form-input-styled" required data-fouc>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="ml-auto px-2">
                                    <button type="submit" class="btn bg-blue ml-3">Yeni Topic</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
        <!-- /simple lists -->
    </div>
</div>
<!-- /main content -->
{% endblock content %}

@endsection