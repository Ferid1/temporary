@extends('base')

@section('content')
    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><span class="font-weight-semibold">Topic</span> - Page</h4>
                </div>
            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/rules" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Rule</a>
                        <span class="breadcrumb-item active">Page</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page header -->

        <div class="content">
            <!-- Simple lists -->
            <div class="row">
                <div class="col-md-12">

                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-white header-elements-inline">
                            <h6 class="card-title">Yeni Page</h6>
                        </div>
                        <div class="card-body">
                            <form action="{{ action('TblRulesController@update') }}" method="post">
                            {{ csrf_field() }}
                            <!-- action('TblRulesController@edit') -->
                                    <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                       
                                                    
                                                    <label>Domanin name</label>
                                                    <select name='domain_id' class="form-control" value=''>
                                                    @foreach($domains as $domain)
                                                        <option value="{{$domain->id}}">{{$domain->name}}</option>
                                                    @endforeach
                                          
                                                    </select>
                                                    <br>
                                                    <label>Title</label>
                                                    <input type="hidden" name="id" value="{{ $rules->id}}">
                                                    <input type="text" class="form-control" name="title"  value='{{$rules->rule_title}}'>
                                                    <br>
                                                    <label>Short content</label>
                                                    <input type="text" class="form-control" name="short_content" value='{{$rules->rule_short_content}}'>
                                                
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">    
                                                    <br>       
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-lg-2">Image:</label>
                                                        <div class="col-lg-10">
                                                            <input type="file" name="styled_file" class="form-input-styled" data-fouc>
                                                        </div>
                                                    </div>
        
                                                    <label>Author</label>
                                                    <input type="text" class="form-control" name="author" value='{{$rules->rule_author}}'>
                                                
                                                    <br>
                                                    <label for="start">Start date:</label>
        
                                                    <input name='full_date' class="form-control" type="date" id="start" name="date"
                                                        value="{{$rules->rule_date_full}}"
                                                        min="2018-01-01" max="2018-12-31">                                                                       
                                                </div>
                                            </div>
                                        </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn bg-teal">Yeni Page</button>
                                    </div>
                                </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /simple lists -->
        </div>

        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text text-center">&copy; 2019</span>
            </div>
        </div>
        <!-- /footer -->

    </div>
    <!-- /main content -->
    @endsection
