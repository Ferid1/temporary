@extends('base')

@section('content')
    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><span class="font-weight-semibold">Rule</span> - Page</h4>
                </div>
            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/rules" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Rule</a>
                        <span class="breadcrumb-item active">Page</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page header -->

        <div class="content">
            <!-- Simple lists -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Dropdown list -->
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Mövcud Level</h5>
                        </div>

                        <div class="card-body">
                            <ul class="media-list">
                            @foreach($rules as $rule)
                                <li class="media justify-content-between">
                                    <div class="box-content">
                                        <span class="mr-3">
                                            <a href="#">
                                                <a href="/rules/{{$rule->id}}" class="row-level">#{{$rule->id}}</a>
                                            </a>
                                        </span>

                                        <span class="media-body mr-3">
                                            <a href="/rules/{{$rule->id}}"><span class="media-title font-weight-semibold">{{$rule->name}}</span></a>
                                        </span> 

                                        <span class="media-body mr-3">
                                             <a href="/rules/{{$rule->id}}"><span class="media-title font-weight-semibold">{{$rule->rule_title}}</span></a>
                                        </span> 

                                        <span class="media-body mr-3">
                                             <a href="/rules/{{$rule->id}}"><span class="media-title font-weight-semibold">{{$rule->rule_short_content}}</span></a>
                                        </span> 

                                        <span class="media-body mr-3">
                                             <a href="/rules/{{$rule->id}}"><span class="media-title font-weight-semibold">{{$rule->rule_author}}</span></a>
                                        </span> 

                                        <span class="media-body mr-3">
                                             <a href="/rules/{{$rule->id}}"><span class="media-title font-weight-semibold">{{$rule->rule_date_full}}</span></a>
                                        </span> 
                                    </div>
                                    <div class="box-buttons">
                                        <span class="mr-2">
                                            <a href="/rules/{{$rule->id}}/edit">
                                                <i class="icon-pencil7 icon-1x" data-popup="tooltip" title="" data-original-title="Redaktə et"></i>
                                            </a>
                                        </span>
                                        <span class="mr-2">
                                            <a href="/rules/{{$rule->id}}/remove">
                                                <i class="icon-trash icon-1x text-danger"  data-popup="tooltip" title="" data-original-title="Sil"></i>
                                            </a>
                                        </span>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /dropdown list -->

                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-white header-elements-inline">
                            <h6 class="card-title">New Rule</h6>
                        </div>
                        <div class="card-body">
                            <form action="{{ action('TblRulesController@store') }}" method="post">
                            {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Domanin name</label>
                                            <select name='domain_id' class="form-control">
                                                @foreach($domains as $domain)
                                                <option value="{{$domain->id}}">{{$domain->name}}</option>
                                                @endforeach
                                            </select>
                                            <br>                                        
                                            <label>Title</label>
                                            <div class="container1_title">
                                                <div><input type="text" class='multiSelect' name="title"></div>
                                            </div>
                                            <br>

                                            <label>Short content</label>
                                            <div class="container1_content">
                                                <div><input type="text" class='multiSelect' name="content"></div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">    
                                            <br>                                    
                                            <label>Image</label>
                                            <div class="container1_img">
                                                <div><input type="file" class='multiSelect' name="styled_file"></div>
                                            </div> <br>


                                            <label>Author</label>
                                            <div class="container1_author">
                                                <div><input type="text" class='multiSelect' name="author"></div>
                                            </div>
                                            
                                            <br>
                                            <label>Date</label>
                                            <div class="container1_date">
                                                <div><input type="date" class='multiSelect' name="date"></div>
                                            </div>

                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn bg-teal">New rule</button>
                                    </div>
                                </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /simple lists -->
        </div>

        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text text-center">&copy; 2019</span>
            </div>
        </div>
        <!-- /footer -->

    </div>
    <!-- /main content -->
    @endsection
