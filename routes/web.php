<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {});

// Route::get('domains/pakets','PaketController@index');
// Route::get('panel/backgrounds','BackgroundController@index');
// Route::get('panel/', 'TopicController@index');

Route::get('/', 'DomainController@index');
Route::get('domains', 'DomainController@domain');
Route::get('domains/remove/{id}','DomainController@destroy');
Route::post('domains/add','DomainController@store');
Route::get('domains/{id}/edit','DomainController@showEdit');
Route::post('domains/update','DomainController@update');

Route::get('rules/', 'TblRulesController@index');
Route::post('rules/add','TblRulesController@store');
Route::get('rules/{id}/edit','TblRulesController@edit');
Route::get('rules/{id}/remove','TblRulesController@destroy');
Route::post('rules/update','TblRulesController@update');

Route::get('/api/rules', "ApiController@allRules");
Route::get('/api/rules/{id}', "ApiController@singleRule");


Route::get('/api/domains', "ApiController@allDomains");
Route::get('/api/domains/{id}', "ApiController@singleDomain");

// Route::get('/api/topics/{id}/sub-topics', "ApiController@topicSubs");
// Route::get('/api/sub-topics/{id}', "ApiController@subsQuestion");

// Route::get('/api/packets/{id}', "ApiController@paketsOne");
// Route::get('/api/backgrounds', "ApiController@backgrounds");
